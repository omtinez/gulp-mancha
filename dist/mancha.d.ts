/// <reference types="node" />
import * as stream from 'stream';
declare module Mancha {
    /**
     * Helper function used to escape HTML attribute values.
     * See: https://stackoverflow.com/a/9756789
     */
    function encodeHtmlAttrib(value: string): string;
    /** Inverse the operation of [encodeHtmlAttrib] */
    function decodeHtmlAttrib(value: string): string;
    function preprocess(content: string, vars: {
        [key: string]: string;
    }, root: string, wwwroot: string, encoding: BufferEncoding): string;
    function render(content: string, vars?: {
        [key: string]: string;
    }, context?: {
        [key: string]: any;
    }, root?: string, wwwroot?: string, encoding?: BufferEncoding): Promise<string>;
}
/**
 * Main entrypoint to be used in Gulp. Usage:
 *
 *     var mancha = require('gulp-mancha')
 *     gulp.src(...).pipe(mancha({myvar: myval})).pipe(...)
 *
 * @param vars <key, value> pairs of literal string replacements. `key` will become `{{key}}` before
 * replacing it with `value` in the processed files.
 *
 * @param context <key, value> pairs of objects passed to the sandbox. By default, the following
 * objects are available to rendering scripts:
 *
 * + `document`: buffer used for inserting into the DOM. It's not a virtual DOM, only a dummy object
 *   with method `write()` which inserts string content to the DOM.
 * + `__file`: path of the file being rendered, which might not be the same as the file containing
 *   the script due to includes.
 */
declare function mancha(vars?: {
    [key: string]: string;
}, context?: {
    [key: string]: any;
}, wwwroot?: string): stream.Transform;
declare namespace mancha {
    var encodeHtmlAttrib: typeof Mancha.encodeHtmlAttrib;
    var decodeHtmlAttrib: typeof Mancha.decodeHtmlAttrib;
}
export = mancha;
